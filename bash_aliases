# drush; to check the version in use, see the symlink called here.
alias drush="/opt/drush"

# sublime_text shortcuts.
alias subl="/opt/Sublime Text 2/sublime_text -a"
alias subw="/opt/Sublime Text 2/sublime_text -w"

# This is only here until this issue is resolved.
# http://sublimetext.userecho.com/topic/27820
alias svim="sudo vim";

# General ip address shortcuts
alias ip-int='ifconfig | grep "inet " |grep -v "127.0.0.1" | cut -d: -f2 | cut -d\  -f1';
alias ip-ext-c="wget http://automation.whatismyip.com/n09230945.asp -O - -o /dev/null"
alias ip-ext="ip-ext-c && echo";
alias ip-all="echo -n 'Internal: ' && ip-int && echo -n 'External: ' && ip-ext";

# apt-get shortcuts
alias install="sudo apt-get install";
alias remove="sudo apt-get remove --purge";
alias update="sudo apt-get update && sudo apt-get upgrade";

# listing...
# Basic
alias la="ls -GhAl --color=auto";
# Directories only simplified
alias lsd="ls -p --color=auto | grep / | cut -d/ -f1"
# Directories only extended
alias lad="la -p | grep / | cut -d/ -f1";
# Files only simplified
alias lsf="ls -p --color=auto | grep -v /"
# Files only extened
alias laf="la -p | grep -v /"

# ssh shortcuts... paired with ssh-keys to optomize workflow
alias ssh:np="ssh nanek30@shell.newpaltz.edu";
alias ssh:home="ssh -p2222 tnanek@stormville.dyndns-web.com";
alias ssh:sa="ssh -p2222 admin@newpaltzsa.com";
alias ssh:sa-dev="ssh aegir@newpaltzsa.dev";
alias ssh:my="ssh keenduon@keen-duo.net";

# extract bash script
ex (){
  if [ -f "$1" ] ; then
    case "$1" in
      *.tar.bz2)   tar xjf $1        ;;
      *.tar.gz)    tar xzf $1     ;;
      *.bz2)       bunzip2 $1       ;;
      *.rar)       rar x $1     ;;
      *.gz)        gunzip $1     ;;
      *.tar)       tar xf $1        ;;
      *.tbz2)      tar xjf $1      ;;
      *.tgz)       tar xzf $1       ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1  ;;
      *.7z)        7z x $1    ;;
      *)           echo "'$1' cannot be extracted via extract(" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# Easy ways to switch versions of drush... next is adding a switch of which drush.php is in use.
# alter-drush-4
alias alter-drush-4="sudo rm /opt/drush; sudo ln -s /opt/drush-4.x/drush /opt/drush"
# alter-drush-5
alias alter-drush-5="sudo rm /opt/drush; sudo ln -s /opt/drush-5.x/drush /opt/drush"
# alter-drush-release
alias alter-drush-release="sudo rm /opt/drush; sudo ln -s /opt/drush-release/drush /opt/drush"
