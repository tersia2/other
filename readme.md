Where to put these files
========================

Every file here resides at a certain location on the system.

Generally, the best idea is to establish symlinks from those
locations to this one.

* ~/.config/terminator/config -> terminator-config 
* ~/.bashrc -> bashrc
* ~/.bash_aliases -> bash_aliases
